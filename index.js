const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");


/*txtFirstName.addEventListener('input', handleInput => {
	spanFullName.innerHTML = txtFirstName.value
});

txtLastName.addEventListener('input', handleInput => {
	spanFullName.innerHTML = txtLastName.value
}); 
*/
function updateFullName() {
  const fullName = `${txtFirstName.value} ${txtLastName.value}`;
  spanFullName.innerHTML = fullName;
}

txtFirstName.addEventListener('input', updateFullName);
txtLastName.addEventListener('input', updateFullName);